<?php

require('config.php');

$stravaOauthURL = 'https://www.strava.com/oauth/authorize?' .
    'client_id=' . STRAVA_CLIENT_ID . '&' .
    "redirect_uri={$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}/auth/gettoken.php" . '&' .
    'response_type=code' . '&';
if (defined('STRAVA_SCOPE')) {
    $stravaOauthURL .= 'scope=' . STRAVA_SCOPE . '&';
}

?><!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" 
        href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css">
</head>
<body>
    <a href="<?php echo $stravaOauthURL; ?>" class="display-1">Log In with Strava</a>
</body>
</html>
