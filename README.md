## Authentiation Procedure

1. Goto https://www.strava.com/oauth/authorize with query parameters

* client_id=
* redirect_uri=
* response_type=code
* [optional] approval_prompt=force

2. Strava will redirect back to our specified redirect_uri with code parameter

3. Fetch accessToken and user information from https://www.strava.com/oauth/token
with query parameters

* client_id=
* client_secret=
* code=

4. Strava will return in JSON format

## API

1. Athlete Activities

* URL: https://www.strava.com/api/v3/athlete/activities
* access_token=
* per_page=
* before=

## API Rate Limit

* 600 per 15-minute (40 per minute)
* 30,000 per day
* Will be return in every API response header (X-RateLimit-Limit, X-RateLimit-Usage)