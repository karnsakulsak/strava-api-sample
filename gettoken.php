<?php

require('config.php');

$code = $_REQUEST['code'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://www.strava.com/oauth/token");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
        'client_id=' . STRAVA_CLIENT_ID . '&' .
        'client_secret=' . STRAVA_CLIENT_SECRET . '&' .
        'code=' . $code);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$output = curl_exec($ch);

curl_close ($ch);

$userInfo = json_decode($output, TRUE);

function printElapsedTime($totalSec) {
    printf("%02d:%02d:%02d", floor($totalSec / 3600), floor(($totalSec % 3600) / 60), $totalSec % 60);
}

if (isset($userInfo['athlete'])) {
    file_put_contents('data/user_' . $userInfo['athlete']['id'], $output);

    $output = file_get_contents('https://www.strava.com/api/v3/athlete/activities?per_page=10&access_token=' . $userInfo['access_token']);
    $activities = json_decode($output, TRUE);


?><!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css">
</head>
<body>
    <div class="p-3">
        <div class="display-3">Welcome <?php echo $userInfo['athlete']['firstname'], ' ', $userInfo['athlete']['lastname']; ?></div>
        <div><img src="<?php echo $userInfo['athlete']['profile']; ?>" /></div>
        <div>
            <div>Your Most Recent Activities</div>
            <hr />
            <?php foreach($activities as $activity): ?>
            <div>Name: <?php echo $activity['name']; ?></div>
            <div>Type: <?php echo $activity['type']; ?></div>
            <div>Start Time: <?php echo $activity['start_date_local']; ?></div>
            <div>Distance: <?php printf('%.3f', $activity['distance'] / 1000); ?> km</div>
            <div>Moving Time: <?php printElapsedTime($activity['moving_time']); ?></div>
            <div>Elapsed Time: <?php printElapsedTime($activity['elapsed_time']); ?></div>
            <div>Elevation Gain: <?php printf('%.1f', $activity['total_elevation_gain']); ?> m</div>
            <hr />
            <?php endforeach; ?>
        </div>
    </div>
</body>
</html>
<?php

} else {

?><!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css">
</head>
<body>
  <div class="display-3 text-danger">Authentication Fail</div>
</body>
</html>
<?php
}
?>

